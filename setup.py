from distutils.core import setup

setup(name='Dice',
      version='1.0',
      description='Dice',
      author='Simon Murphy (GreatIrishElk)',
      author_email='murphysimon@oulook.com@outlook.com',
      url='https://gitlab.com/GreatIrishElk/dice',
      packages=['dice'],
      )
