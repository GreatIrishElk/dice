from enum import Enum
import random


class Die:

    def __init__(self, number_of_sides):
        self.number_of_sides = number_of_sides

    def __repr__(self):
        return f"d{self.number_of_sides}"


class Dice(Enum):
    D_FOUR = Die(4)
    D_SIX = Die(6)
    D_EIGHT = Die(8)
    D_TEN = Die(10)
    D_TWENTY = Die(20)
    D_HUNDRED = Die(100)


def roll_dice(dice: Dice = Dice.D_HUNDRED):
    return random.randint(1, dice.value.number_of_sides)
